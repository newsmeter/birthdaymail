﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayMail
{
    public class PersonLogic
    {
        public List<Person> PersonList()
        {
            List<Person> people = new List<Person>();
            //people.Add(new Person { Id = 1, Name = "Hasan Can", SurName = "Güler", BirthDate = DateTime.Now, Mail = "hasancan.guler@habermetre.com" });
            people.Add(new Person { Id = 1, Name = "Meltem", SurName = "Çağlayan", BirthDate = DateTime.Now, Mail = "meltem@arnasagro.com" });

            return people;
        }
    }
}
