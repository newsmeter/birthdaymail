﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayMail
{
    public class SendMail : IMessenger
    {
        string From = "ik@habermetre.com";
        private string _To;
        private string _Name;
        private string _SurName;
        private string _Subject { get; set; }
        private string _Body { get; set; }

        public SendMail(string To, string Name, string SurName,string Subject, string Body)
        {
            _To = To;
            _Name = Name;
            _SurName = SurName;
            _Subject = Subject;
            _Body = Body;
        }

        public void SendMessage()
        {
            string Host = "smtp.gmail.com";
            SmtpClient client = new SmtpClient(Host, 587);
            client.EnableSsl = true;
            client.Credentials = new NetworkCredential("ik.yenimedya@gmail.com", "YeniMedya-23");

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(From,"Yeni Medya İnsan Kaynakları");
            mailMessage.To.Add(_To);

            mailMessage.Body = _Body;
            mailMessage.Subject = _Subject;

            mailMessage.IsBodyHtml = true;
            client.Send(mailMessage);

            Console.WriteLine(_To + " " + _Name + " " + _SurName + " " + " Mail Gönderildi.");
        }

    }
}
