﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayMail
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string  SurName { get; set; }
        public string Mail { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
